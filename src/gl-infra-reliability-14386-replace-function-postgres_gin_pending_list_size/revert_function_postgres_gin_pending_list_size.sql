/*
Issue: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/14386

This is the rollback script to revert to the previous version of function "postgres_gin_pending_list_size()".
*/

BEGIN
;

/*
Because this is a set-returning function with an associated pg_type for its result rows, we must
explicitly DROP FUNCTION to implicitly remove its old type, rather than just CREATE OR REPLACE FUNCTION.
*/

DROP FUNCTION public.postgres_gin_pending_list_size()
;

/*
Previous version of the function, as returned by:
  SELECT pg_get_functiondef('public.postgres_gin_pending_list_size'::regproc) ;
which was introduced by:
  https://gitlab.com/gitlab-com/gl-infra/production/-/blob/master/src/gl-infra-5849-create-postgres_gin_pending_list_size-function/create-function-postgres_gin_pending_list_size.sql
*/

CREATE OR REPLACE FUNCTION public.postgres_gin_pending_list_size()
  RETURNS TABLE(index_name text, pending_pages bigint)
  LANGUAGE sql
  SECURITY DEFINER
AS $function$
  WITH cte_gin_info AS (
    SELECT i.relname AS relname
    FROM pg_index ix
      LEFT JOIN pg_class i ON ix.indexrelid = i.oid
      LEFT JOIN pg_class t ON ix.indrelid = t.oid
      LEFT JOIN pg_am a ON i.relam = a.oid
    WHERE amname = 'gin'
  )
  SELECT relname::text AS index_name, n_pending_pages * current_setting('block_size')::bigint AS pending_pages
  FROM cte_gin_info, gin_metapage_info(get_raw_page(relname, 0));
$function$
;

/*
Function "postgres_gin_pending_list_size()" is declared as a security-definer function with its owner set to
a postgres superuser because it wraps "get_raw_page()", which can only be called by a superuser.
*/

ALTER FUNCTION public.postgres_gin_pending_list_size() OWNER TO "gitlab-superuser"
;

COMMIT
;
