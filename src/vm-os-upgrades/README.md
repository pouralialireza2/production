# VM OS Upgrade Tools & Automation

[[_TOC_]]

## Overview

This directory contains a collection of scripts, utilities, templates, and documentation to support the regular exercise of ensuring that all applicable compute systems used to host or support gitlab.com are running current, supported versions of their operating system. This overall system will be developed and maintained iteratively, over time.

## Planning

Below we document the overall process we use to plan, test, schedule, conduct, audit, and monitor the work necessary to maintain OS versions across the fleet. This process and documentation will be revisited bi-annually, as we conduct upgrades every two years in the fall, following the latest [Ubuntu LTS release](https://ubuntu.com/about/release-cycle).

### PGBouncer

The PGBouncer fleet is used to load-balance traffic from application pods to our database clusters. In order to upgrade the pgbouncer VMs, we need to gracefully drain traffic to each pgbouncer instance in-turn, so that we minimize or prevent impacting any active user sessions.

#### Inventory

This list of nodes was [manually compiled](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/13520#note_764426645), and is accurate as of 2021-12-21.

```
# gstg
pgbouncer-01-db-gstg.c.gitlab-staging-1.internal
pgbouncer-02-db-gstg.c.gitlab-staging-1.internal
pgbouncer-03-db-gstg.c.gitlab-staging-1.internal
pgbouncer-sidekiq-01-db-gstg.c.gitlab-staging-1.internal
pgbouncer-sidekiq-02-db-gstg.c.gitlab-staging-1.internal
pgbouncer-sidekiq-03-db-gstg.c.gitlab-staging-1.internal

# gprd
pgbouncer-01-db-gprd.c.gitlab-production.internal
pgbouncer-02-db-gprd.c.gitlab-production.internal
pgbouncer-03-db-gprd.c.gitlab-production.internal
pgbouncer-sidekiq-01-db-gprd.c.gitlab-production.internal
pgbouncer-sidekiq-02-db-gprd.c.gitlab-production.internal
pgbouncer-sidekiq-03-db-gprd.c.gitlab-production.internal

# db-benchmarking
ci-pgbouncer-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal
ci-pgbouncer-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal
ci-pgbouncer-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal
ci-pgbouncer-sidekiq-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal
ci-pgbouncer-sidekiq-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal
ci-pgbouncer-sidekiq-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-as-test-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-as-test-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-as-test-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-registry-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-registry-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-registry-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-sidekiq-01-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-sidekiq-02-db-db-benchmarking.c.gitlab-db-benchmarking.internal
pgbouncer-sidekiq-03-db-db-benchmarking.c.gitlab-db-benchmarking.internal
```