#! /usr/bin/env bash

set -euo pipefail

set -x

echo "Confirm that persistent volumes claims have been resized to their new specified size:"
kubectl --namespace=monitoring get pvc --selector=app.kubernetes.io/name=thanos-store --output=wide

read -p "Continue? (y/N)" -n 1 -r
echo
[[ $REPLY =~ ^[Yy]$ ]] || exit

echo "Confirm inside each pod that the new volume mount shows more size:"
for i in $(seq 0 14); do
    kubectl --namespace=monitoring exec --stdin --tty thanos-store-$i-0 -- sh -c 'df -h /var/thanos/store'
    kubectl --namespace=monitoring exec --stdin --tty thanos-store-$i-1 -- sh -c 'df -h /var/thanos/store'
done

read -p "Continue? (y/N)" -n 1 -r
echo
[[ $REPLY =~ ^[Yy]$ ]] || exit

echo "Confirm that all statefulset objects exist:"
kubectl --namespace=monitoring get sts/thanos-store-0
kubectl --namespace=monitoring get sts --selector=app.kubernetes.io/name=thanos-store
