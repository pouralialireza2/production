#! /usr/bin/env bash

set -euo pipefail

set -x

# Confirm only two pvcs and their size
kubectl --namespace=monitoring get pvc --selector=app.kubernetes.io/name=thanos-store -o wide

# Patch both persistent volume claims
for i in $(seq 0 14); do
    kubectl --namespace=monitoring patch pvc/data-thanos-store-$i-0 --patch='{ "spec": { "resources": { "requests": { "storage": "45Gi" }}}}'
    kubectl --namespace=monitoring patch pvc/data-thanos-store-$i-1 --patch='{ "spec": { "resources": { "requests": { "storage": "45Gi" }}}}'
done
