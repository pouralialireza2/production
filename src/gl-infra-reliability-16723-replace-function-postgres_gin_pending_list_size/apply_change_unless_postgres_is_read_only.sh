#!/bin/bash

set -e

# Usage: Set the variables for the change dir and SQL file, and then run this script locally on each relevant host.
#
# Example:
# Generate a host list for all chef nodes using the `gitlab-exporter::postgres_exporter` recipe in the `gstg` environment:
# $ knife search -i 'environment:gstg AND recipes:gitlab-exporters\:\:postgres_exporter' | sort -V > host_list.postgres_exporter.gstg.txt
# Run the apply script on those hosts:
# $ mussh -m10 -b -H host_list.postgres_exporter.gstg.txt -C apply_change_unless_postgres_is_read_only.sh

BRANCH='master'
CHANGE_DIR='production/src/gl-infra-reliability-16723-replace-function-postgres_gin_pending_list_size/'
SQL_FILE='upgrade_function_postgres_gin_pending_list_size.sql'
#SQL_FILE='revert_function_postgres_gin_pending_list_size.sql'

function main()
{
    exit_if_postgres_is_read_only
    clone_repo
    apply_change
    test_change
}

function exit_if_postgres_is_read_only()
{
    IS_POSTGRES_READONLY=$( sudo gitlab-psql -AXqt -c 'select pg_is_in_recovery()' )
    if [[ "$IS_POSTGRES_READONLY" == "t" ]] ; then
        echo "Skipping.  Postgres is read-only."
        exit 0
    elif [[ "$IS_POSTGRES_READONLY" == "f" ]] ; then
        echo "Proceeding.  Postgres is writable."
    else
        echo "ERROR: Cannot determine postgres status."
        exit 1
    fi
}

function clone_repo()
{
    rm -rf production
    git clone -q --depth 1 --branch "$BRANCH" https://gitlab.com/gitlab-com/gl-infra/production.git
    cd "$CHANGE_DIR"
}

function apply_change()
{
    echo "$( date -u +%Y-%m-%d\ %H:%M:%S\ %Z )  Applying change: $SQL_FILE"
    sudo gitlab-psql < "$SQL_FILE"
}

function test_change()
{
    echo "$( date -u +%Y-%m-%d\ %H:%M:%S\ %Z )  Verifying change."
    sudo gitlab-psql -AXqt -c 'select count(1) >= 0 from postgres_gin_pending_list_size()'
}

main "$@"
