/*
Issue: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16723

This is the rollback script to revert to the previous version of function "postgres_gin_pending_list_size()".
To apply, run this SQL script using gitlab-psql.
*/

BEGIN
;

/*
Because this is a set-returning function with an associated pg_type for its result rows, we must
explicitly DROP FUNCTION to implicitly remove its old type, rather than just CREATE OR REPLACE FUNCTION.
*/

DROP FUNCTION public.postgres_gin_pending_list_size()
;

/*
Previous version of the function, as returned by:
  SELECT pg_get_functiondef('public.postgres_gin_pending_list_size'::regproc) ;
which was introduced by:
  https://gitlab.com/gitlab-com/gl-infra/production/-/blob/master/src/gl-infra-reliability-14386-replace-function-postgres_gin_pending_list_size/upgrade_function_postgres_gin_pending_list_size.sql
*/

CREATE OR REPLACE FUNCTION public.postgres_gin_pending_list_size()
  RETURNS TABLE(index_name text, pending_list_bytes bigint)
  LANGUAGE sql
  SECURITY DEFINER
AS $function$
  WITH gin_indexes AS (
    SELECT ix.indexrelid
    FROM pg_index ix
      LEFT JOIN pg_class i ON ix.indexrelid = i.oid
      LEFT JOIN pg_am a ON i.relam = a.oid
    WHERE a.amname = 'gin' AND i.relkind = 'i'
  )
  SELECT indexrelid::regclass::text AS index_name, n_pending_pages * current_setting('block_size')::bigint AS pending_list_bytes
  FROM gin_indexes, gin_metapage_info(get_raw_page(indexrelid::regclass::text, 0))
$function$
;

/*
Function "postgres_gin_pending_list_size()" is declared as a security-definer function with its owner set to
a postgres superuser because it wraps "get_raw_page()", which can only be called by a superuser.
*/

ALTER FUNCTION public.postgres_gin_pending_list_size() OWNER TO "gitlab-superuser"
;

COMMIT
;
