# GitLab Production

The GitLab Production repository is to track issues for incidents and change management.

## Incidents

https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/

Incidents are anomalous conditions that result in service degradation or outage and require intervention (human or automated) to restore service to full operational status in the shortest amount of time possible. The primary goal of incident management is to organize chaos into swift incident resolution. To that end, incident management requires well defined roles for all resources involved, control points to manage the flow of both resolution path and information, active and effective communication to notify the appropriate stakeholders about the status of an incident and its resolution, and post-mortem, root-cause analysis and introspective analysis procedures.

## Change Management

https://about.gitlab.com/handbook/engineering/infrastructure/change-management/

Changes are any modification to the operational environment and are classified into two types:

* **Service changes** are regular, routine changes executed through well-tested, automated procedures performed with minimal human interaction that may cause predictable and limited performance degradation and no downtime. As such, service changes do not require review or approval except on their very first iteration.
* **Maintenance changes** are possibly complex changes that require manual intervention beyond initiating the change and that will cause downtime or significant performance degradation by the nature of the change. These changes require strict scheduling, careful planning and review, and approval by the Director of Infrastructure for execution.

Deployments are a special change metatype depending on their scope and the effect they may have on the environment, as defined above. As we make progress towards CI/CD, we aim to turn all deployments into simple service changes.

### Other useful links

* [GitLab Runbooks](https://gitlab.com/gitlab-com/runbooks)
* [Other infrastructure related issues](https://about.gitlab.com/handbook/engineering/infrastructure/)
* [Production Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production-architecture/)
* [GitLab Monitoring](https://about.gitlab.com/handbook/engineering/monitoring/).

# Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
