# frozen_string_literal: false

RSpec.configure do |config|
  config.success_color = :cyan
end
