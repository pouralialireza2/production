---

.projectsOnly: &projectsOnly
  projects:
    only:
      - 7444821
      - 1304532

.rootcauses: &rootcauses
  collection_labels:
    - RootCause::Config-Change
    - RootCause::DB-Migration
    - RootCause::Database-Failover
    - RootCause::External-Dependency
    - RootCause::ExternalAgentMaliciousBehavior
    - RootCause::FalseAlarm
    - RootCause::Feature-Flag
    - RootCause::Flaky-Test
    - RootCause::GCP-Networking
    - RootCause::Indeterminate
    - RootCause::Known-Software-Issue
    - RootCause::Malicious-Traffic
    - RootCause::Naive-Traffic
    - RootCause::Release-Compatibility
    - RootCause::SPoF
    - RootCause::Saturation
    - RootCause::Security
    - RootCause::Software-Change

.services: &services
  collection_labels:
    - Service::API
    - Service::AlertManager
    - Service::Blackbox
    - Service::CI Runners
    - Service::Camoproxy
    - Service::Chef
    - Service::CloudSQL
    - Service::Cloudflare
    - Service::Consul
    - Service::Container Registry
    - Service::Contributors
    - Service::Customers
    - Service::CustomersDot
    - Service::Data-pipelines
    - Service::Deploy-Node
    - Service::DeployTooling
    - Service::Design
    - Service::ExternalDNS
    - Service::Fastly
    - Service::Forum
    - Service::Frontend
    - Service::GCP
    - Service::Gemnasium
    - Service::Geo
    - Service::Git
    - Service::GitLab Rails
    - Service::GitLab SSHD
    - Service::Gitaly
    - Service::Gitlab Shell
    - Service::Gitter
    - Service::Grafana
    - Service::HAProxy
    - Service::Helix
    - Service::Infrastructure
    - Service::KAS
    - Service::Kube
    - Service::License
    - Service::Logging
    - Service::Mailroom
    - Service::Monitoring-Other
    - Service::NAT
    - Service::NGINX
    - Service::Oncall-Tooling
    - Service::PVS
    - Service::PackageCloud
    - Service::Pages
    - Service::Patroni
    - Service::PatroniCI
    - Service::Pgbouncer
    - Service::PipelineValidationService
    - Service::PlantUML
    - Service::Postgres
    - Service::Praefect
    - Service::Prometheus
    - Service::PublicGrafana
    - Service::Redis
    - Service::Runbooks
    - Service::Search
    - Service::Security-response
    - Service::Sentry
    - Service::Sidekiq
    - Service::Snowplow
    - Service::Stackdriver
    - Service::Teleport
    - Service::Thanos
    - Service::Unknown
    - Service::Vault
    - Service::Version
    - Service::Web
    - Service::Websockets
    - Service::Websockets
    - Service::Windows CI Runner
    - Service::Woodhouse
    - Service::Workhorse
    - Service::Zendesk

.severities: &severities
  collection_labels:
    - severity::1
    - severity::2
    - severity::3
    - severity::4

IssueCharts:
  title: Summary of Incidents
  charts:
    ## Root Cause
    - title: Incidents by root cause
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: month
        period_limit: 3
        filter_labels:
          - incident
        <<: *rootcauses
    - title: Incidents in the last week by root cause
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: day
        period_limit: 7
        filter_labels:
          - incident
        <<: *rootcauses

    ## Service
    - title: Incidents by service
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: month
        period_limit: 3
        filter_labels:
          - incident
        <<: *services
    - title: Incidents in the last week by service
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: day
        period_limit: 7
        filter_labels:
          - incident
        <<: *services

    ## Severity
    - title: Incidents by severity
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: month
        period_limit: 3
        filter_labels:
          - incident
        <<: *severities
    - title: Incidents in the last week by severity
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: day
        period_limit: 7
        filter_labels:
          - incident
        <<: *severities

    ## Severity and Service
    - title: S1 Incidents by Service
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: month
        period_limit: 3
        filter_labels:
          - incident
          - severity::1
        <<: *services
    - title: S2 Incidents by Service
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: month
        period_limit: 3
        filter_labels:
          - incident
          - severity::2
        <<: *services
    - title: S3 Incidents by Service
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: month
        period_limit: 3
        filter_labels:
          - incident
          - severity::3
        <<: *services
    - title: S4 Incidents by Service
      type: stacked-bar
      query:
        issuable_type: issue
        issuable_state: all
        group_by: month
        period_limit: 3
        filter_labels:
          - incident
          - severity::4
        <<: *services

CoreInfraUnplanned:
  title: Unplanned measurements for requests coming to Core-Infra
  charts:
    - title: unplanned issues by assisting team
      type: stacked-bar
      <<: *projectsOnly
      query:
        issuable_type: issue
        issuable_state: all
        filter_labels:
          - team::Core-Infra
          - Unplanned
        collection_labels:
          - AssistingTeam::Data
          - AssistingTeam::Development
          - AssistingTeam::Growth&Fullfillment
          - AssistingTeam::IT
          - AssistingTeam::Infrastructure
          - AssistingTeam::Marketing
          - AssistingTeam::Sales
          - AssistingTeam::Security
          - AssistingTeam::Support
        group_by: month
        period_limit: 4

    - title: unplanned issues by assist type
      type: stacked-bar
      <<: *projectsOnly
      query:
        issuable_type: issue
        issuable_state: all
        filter_labels:
          - team::Core-Infra
          - Unplanned
        collection_labels:
          - AssistType::CloudInfra
          - AssistType::DataRestore
          - AssistType::Domain-DNS-SSL-Management
          - AssistType::GitLabAdmin
          - AssistType::HandbookMaintenance
          - AssistType::SaaSQuestion
        group_by: month
        period_limit: 4
