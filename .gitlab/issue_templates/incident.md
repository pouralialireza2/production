/label ~incident ~"Incident::Active"

<!-- CONFIDENTIAL ISSUES: Try to keep the primary incident public. If necessary use a vague title. Any confidential information (summary, timeline, findings) should be contained in a linked, confidential issue. -->

## Current Status

<!-- Leave a brief headline remark so that people know what's going on. It is perfectly acceptable for this to be vague while not much is known.  -->

More information will be added as we investigate the issue.
For customers believed to be affected by this incident, please subscribe to this issue or monitor our status page for further updates.

<!-- woodhouse-exec-summary Uncomment this section for SEV1 and SEV2 incidents

### :pencil: Summary for CMOC notice / Exec summary:

1. Customer Impact: {+ Human-friendly 1-sentence statement on impacted +}
1. Service Impact: {+ service:: labels of services impacted by this incident +}
1. Impact Duration: {+ start time UTC +} - {+ end time UTC +} ({+ duration in minutes +})
1. Root cause: {+ TBD +}

-->
### :books: References and helpful links

**Recent Events (available internally only):**

* [Feature Flag Log](https://nonprod-log.gitlab.net/goto/ef8330a5bf26e872457f7b45983c96df) - [Chatops to toggle Feature Flags Documentation](https://docs.gitlab.com/ee/development/feature_flags/controls.html#rolling-out-changes)
* [Infrastructure Configurations](https://nonprod-log.gitlab.net/goto/08064e2c976475f64c52924e3e9918b0)
* [GCP Events](https://log.gprd.gitlab.net/goto/c7212d51c34e012f7b7926df9a408851) (e.g. host failure)

#### Deployment Guidance

* [Deployments Log](https://nonprod-log.gitlab.net/goto/b20f7f27a257959cbc40d3b60af231ac) | [Gitlab.com Latest Updates](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=dot-com)
* [Reach out to Release Managers for S1/S2 incidents to discuss Rollbacks and/or Hot Patching](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/#reaching-our-team) | [Rollback Runbook](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/rollback-a-deployment.md) | [Hot Patch Runbook](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/post-deployment-patches.md)

**Use the following links to create related issues to this incident if additional work needs to be completed after it is resolved:**

* <!-- woodhouse-related-issue -->[Corrective action](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/new?issuable_template=incident-corrective-action) ❙ <!-- woodhouse-related-issue -->[Infradev](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=InfraDev)
* <!-- woodhouse-related-issue -->[Incident Review](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new?issuable_template=incident_review) ❙ <!-- woodhouse-related-issue -->[Infra investigation followup](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/new?issuable_template=incident-investigation)
* <!-- woodhouse-related-issue -->[Confidential Support contact](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new?issuable_template=confidential_incident_data) ❙ <!-- woodhouse-related-issue -->[QA investigation](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/new?issuable_template=Incident)

----

**Note:**
In some cases we need to redact information from public view. We only do this in a limited number of documented cases. This might include the summary, timeline or any other bits of information, laid out in out [handbook page](https://about.gitlab.com/handbook/communication/#not-public). Any of this confidential data will be in a linked issue, only visible internally.
**By default, all information we can share, will be public**, in accordance to our [transparency value](https://about.gitlab.com/handbook/values/#transparency).

<!-- woodhouse: '/timeline _**Roles and responsibilities** IM: {{ .IMOCMentions }} EOC: {{ .EOCMentions }} Declared by: @{{ .Username }}_' -->
/lock