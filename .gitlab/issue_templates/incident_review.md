/label ~"incident-review"
## Incident Review

<!--

The purpose of this Incident Review is to serve as a classroom to help us better understand the root causes of an incident. Treating it as a classroom allows us to create the space to let us focus on devising the mechanisms needed to prevent a similar incident from recurring in the future. A root cause can **never be a person** and this Incident Review should be written to refer to the system and the context rather than the specific actors. As placeholders for names, consider the usage of nouns like "technician", "engineer on-call", "developer", etc..

_For calculating duration of event, use the [Platform Metrics Dashboard](https://dashboards.gitlab.net/d/general-triage/general-platform-triage?orgId=1) to look at appdex and SLO violations._

-->

**The DRI for the incident review is the issue assignee.**

- [ ] If applicable, ensure that the exec summary is completed at the top of the associated incident issue, the timeline tab is updated and relevant graphs are included.
- [ ] If there are any corrective actions or infradev issues, ensure they are added as related issues to the original incident.
- [ ] Fill out relevant sections below or link to the meeting review notes that cover these topics

### Customer Impact

1. **Who was impacted by this incident? (i.e. external customers, internal customers)**
    1. ...
2. **What was the customer experience during the incident? (i.e. preventing them from doing X, incorrect display of Y, ...)**
    1. ...
3. **How many customers were affected?**
    1. ...
4. **If a precise customer impact number is unknown, what is the estimated impact (number and ratio of failed requests, amount of traffic drop, ...)?**
    1. ...

### What were the root causes?

- ...

### Incident Response Analysis

1. **How was the incident detected?**
    1. ...
1. **How could detection time be improved?**
    1. ...
1. **How was the root cause diagnosed?**
    1. ...
1. **How could time to diagnosis be improved?**
    1. ...
1. **How did we reach the point where we knew how to mitigate the impact?**
    1. ...
1. **How could time to mitigation be improved?**
    1. ...

### Post Incident Analysis

1. **Did we have other events in the past with the same root cause?**
    1. ...
1. **Do we have existing backlog items that would've prevented or greatly reduced the impact of this incident?**
    1. ...
1. **Was this incident triggered by a change (deployment of code or change to infrastructure)? If yes, link the issue.**
    1. ...

### What went well?

<!--
Use this section to highlight what went well during the incident. Capturing this helps understand informal
processes and expertise, and enables undocumented knowledge to be shared.

_example:_
1. We quickly discovered a recently changed feature flag through the event log which enabled fast mitigation of the impact, as well as pulling in the engineer involved to further diagnose.
2. We escalated through dev escalations, which brought in Person X. They knew that Person Y had expertise with the component in question, which enabled faster diagnosis.
3. Person Z discovered a misconfiguration in component A by using a methodology they had used previously. This method was not known by anyone else involved in the incident, and that contribution was crucial to understanding the underlying mechanism of the incident.
-->

- ...

### Guidelines

* [Blameless RCA Guideline](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/workflows/internal/root-cause-analysis.html#meeting-purpose)
